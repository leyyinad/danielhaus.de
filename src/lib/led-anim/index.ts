import danielProfileImg from '$lib/images/daniel-profile-64px.png';
import { image, ledTest, plasma } from './generators';
import { fadeIn, scale, speed, vignette } from './modifiers';
import { add, loop, multiply, sequence, subtract } from './operators';
import { fade } from './transitions';

export * from './generators';
export * from './modifiers';
export * from './operators';

const photo = image(danielProfileImg, {
  width: 64,
  height: 64
});

const logo = scale(
  image('/dh-logo.svg', {
    width: 64,
    height: 64
  }),
  { sx: 0.7 }
);

const scaledLogo = scale(logo, { sx: 0.87 });

export default sequence(
  [fadeIn(vignette(plasma), 1), 1],
  [fade(vignette(plasma), multiply(plasma, photo)), 1],
  [fade(multiply(plasma, photo), photo), 1],
  [
    loop(
      sequence(
        [photo, 10],
        [fade(photo, multiply(plasma, photo)), 2],
        [fade(multiply(plasma, photo), multiply(plasma, scaledLogo)), 2],
        [fade(multiply(plasma, scaledLogo), scaledLogo), 2],
        [scale(scaledLogo, { sx: (t) => Math.cos(t * 5.0), sy: 1.0 }), (2 * Math.PI) / 5.0],
        [fade(scaledLogo, multiply(plasma, scaledLogo)), 2],
        [fade(multiply(plasma, scaledLogo), multiply(plasma, photo)), 2],
        [fade(multiply(plasma, photo), photo), 2],
        [photo, 15],
        [add(photo, speed(ledTest, 40)), 1.6],
        [subtract(photo, speed(ledTest, 80)), 0.8],
        [photo, 60]
      ),
      60
    )
  ]
);
