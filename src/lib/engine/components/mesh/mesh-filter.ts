import type Mesh from '../../geom/mesh';
import Component from '../component';

export default class MeshFilter extends Component {
  mesh!: Mesh;
}
