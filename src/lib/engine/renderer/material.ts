import type Shader from './shader';

export default class Material {
  constructor(public shader: Shader) {}
}
